package shellsort;

import java.util.Arrays;
import java.util.Scanner;

public class Shellsort {
    static Scanner leer=new Scanner(System.in);
    public void ordenar(int[] n) {
      //Ordenar metodo shellshort
      int intervalo;
		intervalo = n.length/2;
		
		while(intervalo>0) {
			for(int i = intervalo; i<n.length; i++) {
				int j=i-intervalo;
				while(j>=0) {
					int k=j+intervalo;
					if(n[j] <= n[k]) {
						j-=1;
					}else {
						int aux = n[j];
						n[j] = n[k];
						n[k] = aux;
						j-=intervalo;
					}
				}
                              
			}
			intervalo=intervalo/2;
		}
          }

public static void main(String[] args) {
      int[] n =new int [5];
      int suma =0,mat=0,leng=0,geom=0,ingl=0,lit=0,media;
      /*Entrada de datos, se asignan a cada elemento
      del array*/
      for (int i = 0; i < 5; i++) {
          System.out.println("Alumno "+(i+1)+" Nota 1: ");
          int a =leer.nextInt();
          System.out.println("Alumno "+(i+1)+" Nota 2: ");
          int b =leer.nextInt();
          System.out.println("Alumno "+(i+1)+" Nota 3: ");
          int c =leer.nextInt();
          System.out.println("Alumno "+(i+1)+" Nota 4: ");
          int d =leer.nextInt();
          System.out.println("Alumno "+(i+1)+" Nota 5: ");
                  
          int e =leer.nextInt();
          
          n[i]=(a+b+c+d+e)/5;
          mat=(mat+a)/2;
          leng=(leng+b)/2;
          geom=(geom+c)/2;
          ingl=(ingl+d)/2;
          lit=(lit+e)/2;
    }
      //sumar todas las notas
      for (int i = 0; i < 5; i++) {
        suma =suma+n[i];
    }
      media=suma/5;
      
      //mostrar media
      System.out.println("Nota media del curso: "+media);
      System.out.println("Nota final de matematicas: "+mat);
      System.out.println("Nota final de lenguaje: "+leng);
      System.out.println("Nota final geometria: "+geom);
      System.out.println("Nota final de ingles: "+ingl);
      System.out.println("Notal final de literatura: "+lit);
      //Mostrando los valores superiores a la media
      System.out.println("Listado de alumnos segun su nota final");
      System.out.println("--------------------------------------");
      Shellsort ss = new Shellsort();
      System.out.println("Lista de alumnos de calificacion desordenados: " + Arrays.toString(n));
      ss.ordenar(n);
      System.out.println("Lista de alumnos de calificacion ordenadas: " + Arrays.toString(n));
      }
          
      }


