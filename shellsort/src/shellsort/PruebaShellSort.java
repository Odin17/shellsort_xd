package shellsort;
import java.util.Arrays;
class Shellsort{
	public void ordenar(int[] numeros) {
		int intervalo;
		intervalo = numeros.length/2;//Comienza dividiendo la dimension del arreglo entre 2--> toma el valor dado mi arreglo
		while(intervalo>0) { //si el intervalo es mayor a 0 entonces entra en el ciclo
			for(int i = intervalo; i<numeros.length; i++) {//comienza en el numero que se dio en la division-->2 hasta el tamaño de la misma que es 4
				int j=i-intervalo;//se asigna una variable una opeeracion aritmetica para que se comience en la primera posicion del arreglo-->0
				while(j>=0) {//cumple la operacion de arriba y entra en el ciclo
					int k=j+intervalo;//aqui hace el salto al la posicion 2 ya que esa operacion le da un valor a k que es 2
					if(numeros[j] <= numeros[k]) {//aqui se comparar las posiciones si la posicion j[0]=12 es menor o igual a k[2]=54 entonces no se hace nada.
						j-=1;// aqui solo si se cumple lo de arriba se pone un numero menor a cero para finalizar el ciclo
					}else {//si no se cumple la condicion de arriba entonces es porque la pocision j[0] es mayor a k[0] y en verdad si es.
						int aux = numeros[j];//aqui el valor j[0]=12 se almacena en el aux
						numeros[j] = numeros[k];//La posicion J quedo vacion pero se le agrega el valor k[2]=54
						numeros[k] = aux;//el valor de k esta vacio y se le agrega el valor de aux que es 12
                                                //entonces asi se hace el cambiando sacando el valor para tener un espacio vacio y poder intercambiarlas
						j-=intervalo;//se descuenta j para que se termine el ciclo mientra y vuelva al for para que siga avanzando por todo el arreglo
					}
				}
			}
			intervalo=intervalo/2;//aqui se llega hasta que se termine de leer el arreglo y hace esta pequeña operacion para entrar de nuevo al for y asi se va hasta llegar al minimo de saltos.
		}
		
	}//public void ordenar
	
}//class shellsort


public class PruebaShellSort {

	public static void main(String[] args) {
		
		
		Shellsort ss = new Shellsort();
		int numeros[] = {23, 54, 4, 10, 1}; 
		System.out.println("Arreglo desordenado: " + Arrays.toString(numeros));
        ss.ordenar(numeros);
		System.out.println("Arreglo ordenado: " + Arrays.toString(numeros));
	}

}
